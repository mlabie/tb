#!/usr/bin/env python

# https://www.thepoorengineer.com/en/arduino-python-plot/

import collections
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import struct
#import pandas as pd


import numpy as np
import sys
import os
import shutil
import datetime
from datetime import timedelta
from datetime import time

from scipy.signal import savgol_filter


def main(argv):
    
    inputfile = ''
    
    if len(sys.argv) != 2:
        print("Error: Please enter the path to the Lightning data file.")
        sys.exit()
    
    inputfile = str(sys.argv[1])
    
    print("File:", inputfile)
    
    dataNumBytes  = 900        # number of bytes of 1 data point
    
    maxPlotLength = dataNumBytes     # size of the plot
    
    
    my_dpi=96
    
    # As it is now, the akunu Lightning sensor sends first a message with the title of the sensor and
    # some of the configuration. As we don't want them to be concidered as Lightning data, we have to check
    # if the file containts this message, in order to remove it. To do so, we will search for the following pattern.
    # everything that comes before and len(...) byte after will be removed. 
    lightning_sensor_string_pattern = "AKUNU Lightning sensor"
    lightning_sensor_byte_pattern = bytearray(lightning_sensor_string_pattern, 'utf-8')
    
    lightning_data = []
    
    tmp = "AKUNU Lightning sensor        %%%%%\r\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\r\nNumber of data before:\t20\r\nNumber of data after:\t880\r\nTotal amount of data:\t900\r\n"

    
    #searching for the string pattern to remove it from the data.
    try:
        with open(inputfile, "rb") as f:
            lightning_raw_data = f.read(dataNumBytes)
            # searching for the offset value of the welcome string
            pattern_pos = lightning_raw_data.find(lightning_sensor_byte_pattern, 0)
                        
            if pattern_pos != -1:
                start_read_offset = pattern_pos + len(tmp)
            else: 
                start_read_offset = 0
                
            
        f.close()
    except IOError: 
        print("Error: could not open/find file.")
        sys.exit()
    
    
    # Creating a folder where to store the graphs. If the folder already exist,
    # clear it.
    try:
        os.mkdir(os.path.splitext(inputfile)[0])
        print(os.path.splitext(inputfile)[0], "directory created.")
        
    except FileExistsError:
        
        # https://stackoverflow.com/questions/185936/how-to-delete-the-contents-of-a-folder-in-python
        for the_file in os.listdir(os.path.splitext(inputfile)[0]):
            file_path = os.path.join(os.path.splitext(inputfile)[0], the_file)
            try:
                if os.path.isfile(file_path):
                    os.unlink(file_path)
                #elif os.path.isdir(file_path): shutil.rmtree(file_path)
            except Exception as e:
                print(e)
                
        print(os.path.splitext(inputfile)[0], "directory cleared.")
    
    
    
    try:
        with open(inputfile, "rb") as f:
            #reading the first "start_read_offset" byte that contains the welcome string
            lightning_raw_data = f.read(start_read_offset)
            
            lightning_raw_data = f.read(dataNumBytes)
            
            cnt = 0
            
            while len(lightning_raw_data) == dataNumBytes:
            
                lightning_data.clear()
                
                min_val = 5
                max_val = -5
                
                
                for byte in lightning_raw_data:
                    lightning_data.append((byte - 128)/25.6)    #converting the value into voltage
                    if (byte - 128)/25.6 < min_val:
                        min_val = (byte - 128)/25.6
                    
                    if (byte - 128)/25.6 > max_val:
                        max_val = (byte - 128)/25.6
                
                plt.figure(figsize=(1600/my_dpi, 900/my_dpi), dpi=my_dpi)
                xmin = 0
                xmax = maxPlotLength
                ymin = min_val
                ymax = max_val
                yticks = (max_val - min_val)/20
                
                y_axis_min_val = min_val - 2*yticks;
                y_axis = [y_axis_min_val + i*yticks for i in range(24)]
                
                #https://pythonspot.com/plot-time-with-matplotlib/
                #customdate = time(12, 31, 23, 50)
                #x_axis = [x_axis_min_val  + 10*i for i in range(int(len(lightning_data)))]
                #print(x_axis)
                #x_axis = [i for i in range(len(lightning_data))]
                
                #print(x_axis)
                
                ax = plt.axes(xlim=(xmin, xmax), ylim=(float(ymin - (ymax - ymin) / 10), float(ymax + (ymax - ymin) / 10)))
                #ax = plt.axes(xlim=(x_axis_min_val, x_axis_max_val), ylim=(float(ymin - (ymax - ymin) / 10), float(ymax + (ymax - ymin) / 10)))
                
                #ax.set_xlim([datetime.date(2014, 1, 26), datetime.date(2014, 2, 1)])
                ax.set_title('Lightning sensor Analog Read - ' + str(cnt))
                ax.set_xlabel("samples")
                ax.set_ylabel("Analog Read Value [V]")
                plt.plot(lightning_data)
                #plt.plot(x_axis, lightning_data)
                #plt.scatter(x_axis,lightning_data)
                #plt.plot_date(x_axis, lightning_data, 'o', True, False)
                #plt.xticks(np.array(x_axis))
                plt.yticks(np.array(y_axis))
                plt.gcf().autofmt_xdate()
                #plt.yticks(np.arange(min_val - 2*yticks, max_val + 2*yticks, yticks))
                #plt.xticks(np.arange(min_val - 2*ticks, max_val + 2*ticks, ticks))
                
                
                plt.grid(color='grey', linestyle=':', linewidth=0.5)
                plt.savefig(str(os.path.splitext(inputfile)[0] + "/" + os.path.splitext(inputfile)[0] + "_" + str(cnt) + ".png"), dpi=my_dpi)
                #plt.show()
                print(os.path.splitext(inputfile)[0] + "_" + str(cnt) + ".png" + " graph created.")
                
                plt.close()
                
                lightning_raw_data = f.read(dataNumBytes)
                cnt += 1

            
        f.close()
    except IOError: 
        print("Error: could not open/find file.")
        sys.exit()
    



if __name__ == '__main__':
    main(sys.argv[1:])
    


