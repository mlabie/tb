/*
 * File:   main.c
 * Author: Marc Labie
 *
 * Created on 5. juillet 2019, 13:39
 */


#include <xc.h>
#include "pll.h"
#include "uart.h"
#include "IO_ports.h"
#include "adc.h"
#include "timer.h"
#include "utils.h"
#include "irq.h"
#include "akunu_lightning_sensor.h"
#include "circular_buffer.h"


int main(void) {
    
    
    char* s  = "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\r\n"
               "%%%%%       AKUNU Lightning sensor        %%%%%\r\n"
               "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%";
    
    uint8_t  state;
    uint32_t time_low;
    uint16_t time_high;
    uint16_t cnt_samples;
    
    char buffer[16];
    
    circular_buf_t cbuf;
    
    unsigned int adc_val; 
    
    pll_init();
    uart_init();
    io_ports_init();
    adc_init();
    timer_init();
    irq_init();
    
    
    enableInterrupts();
    
    led0_on();
    
    uart1_writeln("");
    uart1_writeln(s);
    
    
    uart1_write("Number of data before:\t");
    uitoa(LIGHTNING_DATA_BEFORE, buffer, 10);
    uart1_writeln(buffer);
    
    uart1_write("Number of data after:\t");
    uitoa(LIGHTNING_DATA_AFTER, buffer, 10);
    uart1_writeln(buffer);
    
    
    uart1_write("Total amount of data:\t");
    uitoa(LIGHTNING_DATA_SIZE, buffer, 10);
    uart1_writeln(buffer);
    
    /*
    uart1_writeln("Voltage info:");
    uart1_write("ADC 1.65 V val:\t");
    uitoa(ADC_1_65_V_VAL, buffer, 10);
    uart1_writeln(buffer);
    uart1_write("NEG treshold val:\t");
    uitoa(NEGATIVE_THRESHOLD_VAL, buffer, 10);
    uart1_writeln(buffer);
    uart1_write("POS treshold val:\t");
    uitoa(POSITIVE_THRESHOLD_VAL, buffer, 10);
    uart1_writeln(buffer);
    uart1_write("ADC Voltage offset val:\t");
    uitoa(ADC_VOLTAGE_OFFSET_CORR, buffer, 10);
    uart1_writeln(buffer);*/
    
    //cnt = 0;
    
    timer1_load_period(3600);
    CCP1_load_period(FCY*10);
    CCP2_load_period(FCY/SAMPLING_RATE);
    CCP5_load_period(FCY*15);
    timer1_start();
    CCP1_start();
    CCP2_start();
    CCP3_start();
    CCP4_start();
    CCP5_start();
    
    //enableINT1Interrupts();
    
    
    state = STATE_WAIT;
    cnt_samples = 0;
    circular_buf_init(&cbuf);
    
    // read the holding ADC val.
    adc_val   = ADCBUF0;
    
    while(1){
        
        
        while(!ADSTATLbits.AN0RDY);
        adc_val   = ADCBUF0;
        adc_val >>= 4;
         
        switch(state){
            
            case STATE_WAIT :
                if(adc_val >= NEGATIVE_THRESHOLD_VAL || adc_val <= POSITIVE_THRESHOLD_VAL){
                    // get time
                    time_low  = CCP3_get_time();
                    time_high = timer1_get_time();
                    
                    // Increment the count samples
                    cnt_samples++;
                    
                    // change state
                    state = STATE_SAMPLING;
                }
                
                // add value to circular buffer
                circular_buf_add_val(&cbuf, adc_val);
                
                break;
                
            case STATE_SAMPLING :
                    
                // Increment the count samples
                cnt_samples++;
                
                // add value to circular buffer
                circular_buf_add_val(&cbuf, adc_val);
                
                // if the number of samples sampled equals the number of data
                // needed, than we are done.
                if(cnt_samples == LIGHTNING_DATA_AFTER){
                    state = STATE_DONE;
                }
                
                break;
                
            case STATE_DONE :
                
                // reset the cnt_samples amount to zero
                cnt_samples = 0;
                
                // send the time
                
                // send the datas
                print_circular_buf2(&cbuf);
                
                // reading the holding converted value, to get a new one.
                adc_val   = ADCBUF0;
                
                // go back to state wait again to get new datas.
                state = STATE_WAIT;
                
                break;
        }
         
    }
    
    return 0;
}
