#include "circular_buffer.h"


int circular_buf_init(circular_buf_t* cbuf){
    
    if(!cbuf)
        return -1;
    
    cbuf->pos = 0;
    
    return 0;
}

inline int circular_buf_add_val(circular_buf_t* cbuf, uint8_t val){
    
    if(!cbuf)
        return -1;
    
    
    cbuf->buffer[cbuf->pos] = val;
    
    cbuf->pos = ((cbuf->pos + 1) % CIRCULAR_BUF_SIZE);
    
    return 0;
}


int print_circular_buf(circular_buf_t* cbuf){
    
    uint16_t i, cnt;
    char buffer[8];
    
    if(!cbuf)
        return -1;
    
    i = cbuf->pos;
    
    uart1_writeln("cbuf:");
    
    for(cnt=0; cnt < CIRCULAR_BUF_SIZE; cnt++){
        uitoa(cbuf->buffer[i], buffer, 10);
        uart1_writeln(buffer);
        i = ((i + 1) % CIRCULAR_BUF_SIZE);
    }
    
    return 0;
}


int print_circular_buf2(circular_buf_t* cbuf){
    
    uint16_t i, cnt;
    
    if(!cbuf)
        return -1;
    
    i = cbuf->pos;
    
    
    for(cnt=0; cnt < CIRCULAR_BUF_SIZE; cnt++){
        uart1_putc(cbuf->buffer[i]);
        i = ((i + 1) % CIRCULAR_BUF_SIZE);
    }
    
    return 0;
}