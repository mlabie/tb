#include "pll.h"

void pll_init(){
    
    // code taken from :
    // http://ww1.microchip.com/downloads/en/DeviceDoc/dsPIC33-PIC24-FRM-Oscillator-Module-with-High-Speed-PLL-70005255b.pdf
    // p. 52
    
    // code for 50 MIPS system clock using 8MHz FRC
    // Configure PLL prescaler, both PLL postscalers, and PLL feedback divider
    //CLKDIVbits.PLLPRE   = 1;      // N1=1
    //PLLFBDbits.PLLFBDIV = 125;    // M = 125 
    //PLLDIVbits.POST1DIV = 5;      // N2=5
    //PLLDIVbits.POST2DIV = 1;      // N3=1
    
    // code for 100 MIPS system clock using 8MHz FRC
    // Configure PLL prescaler, both PLL postscalers, and PLL feedback divider
    CLKDIVbits.PLLPRE   = 1;      // N1=1
    PLLFBDbits.PLLFBDIV = 150;    // M = 150
    PLLDIVbits.POST1DIV = 3;      // N2=3
    PLLDIVbits.POST2DIV = 1;      // N3=1
    
    // Initiate Clock Switch to Primary Oscillator with PLL (NOSC=0b011)
    __builtin_write_OSCCONH(0x01);
    __builtin_write_OSCCONL(OSCCON | 0x01);
    // Wait for Clock switch to occur
    while(OSCCONbits.OSWEN != 0);
    // Wait for PLL to lock
    while(OSCCONbits.LOCK!= 1);
}
