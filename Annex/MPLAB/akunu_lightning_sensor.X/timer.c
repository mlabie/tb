#include "timer.h"


volatile unsigned int instructionCount = 0;



void timer_init(){
    
    timer1_init();
    CCP1_timer_init();
    CCP2_timer_init();
    CCP3_timer_init();
    CCP4_timer_init();
    CCP5_timer_init();
    
}

//============================================================================//
//==========              Master Bacis Timer Module                 ==========//
//============================================================================//

void timer1_init(){
    // Taken from :
    // http://ww1.microchip.com/downloads/en/DeviceDoc/dsPIC33-PIC24-FRM-Timer1-Module-70005279B.pdf
    // page 10
    // and modified
    
    T1CON = 0x0;            // Stop timer and clear control register,
                            // set prescaler at 1:1, internal clock source
    
    T1CONbits.TCKPS = 0;    // prescale 1:1
    T1CONbits.TCS   = 1;    // Exertnal clock src
    T1CONbits.TECS  = 0;    //  External Clock comes from the T1CK pin (PPS)
    
    TMR1 = 0x0;             // Clear timer register
}

void timer1_load_period(uint16_t period){
    T1CONbits.TON = 0; // Stop timer
    TMR1 = 0x0;        // Clear timer register 
    PR1  = period;     // Load period register
    T1CONbits.TON = 1; // Start timer
}

uint16_t timer1_get_time(){
    return TMR1;    // return the timer register
}

void timer1_start(){
    T1CONbits.TON = 1; // Start timer
}

void timer1_stop(){
    T1CONbits.TON = 0; // Stop timer
}



//============================================================================//
//==========                1 Second Period Timer                   ==========//
//============================================================================//

void CCP1_timer_init(){
    
    //CCP1CON1Lbits.CCSEL=1;     // Input capture mode
    
    CCP1CON1Lbits.CCSEL=0;      // timer mode
    
    CCP1CON1Lbits.CLKSEL=0;     // Set the clock source (Tcy)
    //CCP1CON1Lbits.T32=0;      // 16-bit Dual Timer mode
    CCP1CON1Lbits.T32=1;        // 32-bit Timer mode
    //CCP1CON1Lbits.MOD= 1;     // Capture ever rising edge of the event
    CCP1CON1Lbits.MOD= 0    ;   // timer mode
    CCP1CON2Hbits.ICSEL= 0;     // Capture rising edge on the Pin
    CCP1CON1Hbits.IOPS=0;       // Interrupt on every input capture event
    CCP1CON1Lbits.TMRPS=0;      // Set the clock pre-scaler (1:1)
    //CCP1CON1Lbits.TMRPS=2;    // Set the clock pre-scaler (1:16)
    CCP1CON1Lbits.TMRSYNC = 0;  // Set timebase synchronization (Synchronized)
    
    CCP1CON1Hbits.ONESHOT = 0;  // One-Shot Triggered mode is disabled(periodic)
    CCP1CON1Hbits.OPSSRC  = 0;
    
    
    //CCP1CON1Hbits.ONESHOT=0;    // One-Shot Triggered mode is disabled(periodic)
   
    // change the source for sync with GPS
    //CCP1CON1Hbits.TRIGEN=1;    // Triggered operation of timer is enabled
    //CCP1CON1Hbits.SYNC=10; // External trigger from INT1 (RB0)
    
    CCP1CON1Hbits.TRIGEN=0;     // Triggered operation of timer is disabled
    CCP1CON1Hbits.SYNC=0;       // No external synchronization; timer rolls over at FFFFh or matches with period register
    
    // if not reseting automaticly
    //CCP1CON2Lbits.PWMRSEN = 1
    
    
    CCP1TMRL = 0x0;     // Set the timer to 0
    CCP1TMRH = 0x0;
    
}



void CCP1_load_period(uint32_t tic){
    CCP1PRL = tic;
    CCP1PRH = (tic>>16);
}



uint32_t CCP1_get_time(){
    uint16_t  timel;
    uint16_t  timeh;
    uint32_t  time;
    
    timel  = CCP1TMRL;
    timeh  = CCP1TMRH;
    time   = timeh;
    time <<= 16;
    time  |= timel;
    return time;
}



void CCP1_start(){
    CCP1CON1Lbits.CCPON=1;     // Enable CCP/input capture
}



void CCP1_stop(){
    CCP1CON1Lbits.CCPON=0;     // disable CCP/input capture
}



//============================================================================//
//==========                  ADC Trigger Timer                     ==========//
//============================================================================//

void CCP2_timer_init(){
    
    // It was mentionned in the manual that is was possible to trigger
    // an A/D convertion using the Timer mode (MOD = 0), but it seems like
    // this was an error in the manual. The Dual Edge Buffered Compare mode
    // contrariwise is working. For more information, please check my thread
    // on the microchip forum:
    // https://www.microchip.com/forums/m1105702.aspx
    
    
    // Taken from:
    // http://ww1.microchip.com/downloads/en/DeviceDoc/30003035b.pdf
    // p.40
    // and modified
    
    CCP2CON1Lbits.CCSEL=0;      // timer mode
    CCP2CON1Lbits.CLKSEL=0;     // Set the clock source (Tcy)
    CCP2CON1Lbits.T32=0;        // 16-bit Dual Timer mode
    CCP2CON1Lbits.MOD= 5;       // Dual Edge Compare mode, buffered mode
    CCP2CON2Hbits.ICSEL= 0;     // Capture rising edge on the Pin
    CCP2CON1Hbits.IOPS=0;       // Interrupt on every input capture event
    CCP2CON1Lbits.TMRPS=0;      // Set the clock pre-scaler (1:1)
    CCP2CON1Lbits.TMRSYNC = 0;  // Set timebase synchronization (Synchronized)
    CCP2CON1Hbits.ONESHOT = 0;  // One-Shot Triggered mode is disabled(periodic)
    CCP2CON1Hbits.TRIGEN=0;     // Triggered operation of timer is disabled
    CCP2CON1Hbits.SYNC=0;       // No external synchronization; timer rolls over
                                // at FFFFh or matches with period register
    
    CCP2TMRL = 0x0;     // Set the timer to 0
    CCP2TMRH = 0x0;
}



void CCP2_load_period(uint16_t tic){
    CCP2PRL = tic;
    CCP2PRH = 0;
    CCP2RA  = 1;
    CCP2RB  = 2;
}



uint16_t CCP2_get_time(){
    return CCP2TMRL;
}



void CCP2_start(){
    CCP2CON1Lbits.CCPON=1;     // Enable CCP/input capture
}



void CCP2_stop(){
    CCP2CON1Lbits.CCPON=0;     // disable CCP/input capture
}


//============================================================================//
//==========           RTC < seconds for lightning sensor           ==========//
//============================================================================//

void CCP3_timer_init(){
    
    CCP3CON1Lbits.CCSEL=0;      // timer mode
    CCP3CON1Lbits.MOD= 0;       // timer mode
    
    // to synchronize with the PPS signal, comment the two lines above, and 
    // uncomment the two ones below
    
    //CCP3CON1Lbits.CCSEL=1;      // capture mode
    //CCP3CON1Lbits.MOD= 1;       // edge detect capture
    CCP3CON1Lbits.CLKSEL=0;     // Set the clock source (Tcy)
    CCP3CON1Lbits.T32=1;        // 32-bit Timer mode
    CCP3CON2Hbits.ICSEL= 0;     // Capture rising edge on the Pin
    CCP3CON1Hbits.IOPS=0;       // Interrupt on every input capture event
    CCP3CON1Lbits.TMRPS=0;      // Set the clock pre-scaler (1:1)
    CCP3CON1Lbits.TMRSYNC = 0;  // Set timebase synchronization (Synchronized)
    CCP3CON1Hbits.ONESHOT = 0;  // One-Shot Triggered mode is disabled(periodic)
    CCP3CON1Hbits.TRIGEN=0;     // Triggered operation of timer is disabled
    CCP3CON1Hbits.SYNC=10;      // Synchronised with the INT1 input (GPS PPS)
    
    CCP3TMRL = 0x0;     // Set the timer to 0
    CCP3TMRH = 0x0;
    CCP3PRL  = 0xFFFF;
    CCP3PRH  = 0xFFFF;
    
}


uint32_t CCP3_get_time(){
    uint16_t  timel;
    uint16_t  timeh;
    uint32_t  time;
    
    timel  = CCP3TMRL;
    timeh  = CCP3TMRH;
    time   = timeh;
    time <<= 16;
    time  |= timel;
    return time;
}



void CCP3_start(){
    CCP3CON1Lbits.CCPON=1;     // Enable CCP/input capture
}



void CCP3_stop(){
    CCP3CON1Lbits.CCPON=0;     // disable CCP/input capture
}


//============================================================================//
//==========           RTC > seconds for lightning sensor           ==========//
//============================================================================//

void CCP4_timer_init(){
    
    
    CCP4CON1Lbits.CCSEL=0;      // Input capture mode
    CCP4CON1Lbits.CLKSEL=0;     // Set the clock source (Tcy)
    CCP4CON1Lbits.T32=1;        // 32-bit Timer mode
    CCP4CON1Lbits.MOD= 0;       // Capture ever rising edge of the event
    CCP4CON2Hbits.ICSEL= 0;     // Capture rising edge on the Pin
    CCP4CON1Hbits.IOPS=0;       // Interrupt on every input capture event
    CCP4CON1Lbits.TMRPS=0;      // Set the clock pre-scaler (1:1)
    CCP4CON1Lbits.TMRSYNC = 0;  // Set timebase synchronization (Synchronized)
    CCP4CON1Hbits.ONESHOT = 0;  // One-Shot Triggered mode is disabled(periodic)
    CCP4CON1Hbits.TRIGEN=0;     // Triggered operation of timer is disabled
    CCP4CON1Hbits.SYNC=0;       // No external synchronization; timer rolls over
                                // at FFFFh or matches with period register
    
    CCP4CON2Lbits.SSDG=1;       // Manually forces auto-shutdown, timer clock 
                                // gate or Input Capture signal gate event 
                                // (setting of ASDGM bit still applies) (gating)
    
    CCP4CON2Lbits.ASDG=(1<<4);  // Master ICMx as gating source
    
    CCP4TMRL = 0x0;     // Set the timer to 0
    CCP4TMRH = 0x0;
    CCP4PRL  = 0xFFFF;
    CCP4PRH  = 0xFFFF;
    
}


uint32_t CCP4_get_time(){
    uint16_t  timel;
    uint16_t  timeh;
    uint32_t  time;
    
    timel  = CCP4TMRL;
    timeh  = CCP4TMRH;
    time   = timeh;
    time <<= 16;
    time  |= timel;
    return time;
}


void CCP4_set_time(uint32_t time){
    CCP4TMRL = time;
    CCP4TMRH = (time>>16);
}


void CCP4_start(){
    CCP4CON1Lbits.CCPON=1;     // Enable CCP/input capture
}



void CCP4_stop(){
    CCP4CON1Lbits.CCPON=0;     // disable CCP/input capture
}


//============================================================================//
//==========                 Periodic ECHO Timer                    ==========//
//============================================================================//

void CCP5_timer_init(){
    
    
    CCP5CON1Lbits.CCSEL=0;      // timer mode
    
    CCP5CON1Lbits.CLKSEL=0;     // Set the clock source (Tcy)
    CCP5CON1Lbits.T32=1;        // 32-bit Timer mode
    CCP5CON1Lbits.MOD= 0    ;   // timer mode
    CCP5CON2Hbits.ICSEL= 0;     // Capture rising edge on the Pin
    CCP5CON1Hbits.IOPS=0;       // Interrupt on every input capture event
    CCP1CON1Lbits.TMRPS=3;      // Set the clock pre-scaler (1:64)
    CCP5CON1Lbits.TMRSYNC = 0;  // Set timebase synchronization (Synchronized)
    
    CCP5CON1Hbits.ONESHOT = 0;  // One-Shot Triggered mode is disabled(periodic)
    CCP5CON1Hbits.OPSSRC  = 0;
    
    CCP5CON1Hbits.TRIGEN=0;     // Triggered operation of timer is disabled
    CCP5CON1Hbits.SYNC=0;       // No external synchronization; timer rolls over at FFFFh or matches with period register
    
    
    CCP5TMRL = 0x0;     // Set the timer to 0
    CCP5TMRH = 0x0;
    
}



void CCP5_load_period(uint32_t tic){
    CCP5PRL = tic;
    CCP5PRH = (tic>>16);
}



uint32_t CCP5_get_time(){
    uint16_t  timel;
    uint16_t  timeh;
    uint32_t  time;
    
    timel  = CCP5TMRL;
    timeh  = CCP5TMRH;
    time   = timeh;
    time <<= 16;
    time  |= timel;
    return time;
}



void CCP5_start(){
    CCP5CON1Lbits.CCPON=1;     // Enable CCP/input capture
}



void CCP5_stop(){
    CCP5CON1Lbits.CCPON=0;     // disable CCP/input capture
}