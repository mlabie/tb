/* 
 * File:   IO_ports.h
 * Author: Marc Labie
 *
 * Created on 10. juillet 2019, 20:35
 */

#ifndef IO_PORTS_H
#define	IO_PORTS_H

#include <xc.h>

// RPx assignement

#define RP32      32        // RB0
#define RP33      33        // RB1
#define RP34      34        // RB2
#define RP35      35        // RB3
#define RP36      36        // RB4
#define RP37      37        // RB5
#define RP38      38        // RB6
#define RP39      39        // RB7
#define RP40      40        // RB8
#define RP41      41        // RB9
#define RP42      42        // RB10
#define RP43      43        // RB11
#define RP44      44        // RB12
#define RP45      45        // RB13
#define RP46      46        // RB14
#define RP47      47        // RB15



#define GPS_PPS_PIN     RP32
#define UART1RX_PIN     RP36
#define UART1TX_PIN     RP37


#define INT1_PIN        (GPS_PPS_PIN)
#define ICM4_PIN        (GPS_PPS_PIN)


/**
 * Initialise all the IO port needed (Calls all the functions below)
 */
void io_ports_init();


//============================================================================//
//==========                      BASIC TIMER1                      ==========//
//============================================================================//

/**
 * Initialise the IO ports for the basic timer1 device
 */
void timer1_ports_init();

//============================================================================//
//==========                        UART1 I/O                       ==========//
//============================================================================//

/**
 * Initialise the IO ports for the UART1 device
 */
void uart1_ports_init();

//============================================================================//
//==========                         ADC I/O                        ==========//
//============================================================================//

void adc_ports_init();

//============================================================================//
//==========                        LED0 I/O                        ==========//
//============================================================================//

/**
 * Initialise the IO ports for the  (on IO RB3)
 */
void led0_ports_init();

/**
 * turns led0 on
 */
void led0_on();


/**
 * turns led0 off
 */
void led0_off();



//============================================================================//
//==========                 External Interrupt PIN                 ==========//
//============================================================================//

void int1_ports_init();


#endif	/* IO_PORTS_H */

