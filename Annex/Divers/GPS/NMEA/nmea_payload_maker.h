/* 
 * File:   nmea_payload_maker.h
 * Author: Marc Labie
 *
 * Created on 31. july 2019
 */

#ifndef NMEA_PAYLOAD_MAKER_H
#define	NMEA_PAYLOAD_MAKER_H

#include <stdint.h>
#include <string.h>



// Please refere to the folowing manual to find the explication on the NMEA
// protocole used here :
// 
// https://www.telit.com/wp-content/uploads/2017/09/Telit_MT_GNSS_User_Guide_r3.pdf


#define DEFUALT_BAUDRATE    9600


#define NMEA_START_OF_DATA_SIZE     sizeof(NMEA_START_OF_DATA)
#define NMEA_END_OF_DATA_SIZE       sizeof(NMEA_END_OF_DATA)
#define NMEA_CR_SIZE                sizeof(NMEA_CR)
#define NMEA_LF_SIZE                sizeof(NMEA_LF)
#define END_OF_LINE_SIZE            sizeof(char)

#define NMEA_CHECKSUM_SIZE          2

#define NMEA_MAX_PACKET_SIZE        82
#define MTK_NMEA_MAX_PACKET_SIZE    255

#define NMEA_CHECKSUM_BUFF_SIZE     (NMEA_CHECKSUM_SIZE + END_OF_LINE_SIZE)

#define NMEA_SENTENCE_SIZE          (NMEA_MAX_PACKET_SIZE - NMEA_START_OF_DATA_SIZE - NMEA_END_OF_DATA_SIZE - NMEA_CR_SIZE - NMEA_LF_SIZE - NMEA_CHECKSUM_SIZE + END_OF_LINE_SIZE)
#define NMEA_PAYLOAD_SIZE           (NMEA_MAX_PACKET_SIZE + END_OF_LINE_SIZE)


#define NMEA_SENTENCE_SIZE          (NMEA_MAX_PACKET_SIZE - NMEA_START_OF_DATA_SIZE - NMEA_END_OF_DATA_SIZE - NMEA_CR_SIZE - NMEA_LF_SIZE - NMEA_CHECKSUM_SIZE + END_OF_LINE_SIZE)
#define NMEA_PAYLOAD_SIZE           (NMEA_MAX_PACKET_SIZE + END_OF_LINE_SIZE)

#define MTK_NMEA_SENTENCE_SIZE      (MTK_NMEA_MAX_PACKET_SIZE - NMEA_START_OF_DATA_SIZE - NMEA_END_OF_DATA_SIZE - NMEA_CR_SIZE - NMEA_LF_SIZE - NMEA_CHECKSUM_SIZE + END_OF_LINE_SIZE)
#define MTK_NMEA_PAYLOAD_SIZE       (MTK_NMEA_MAX_PACKET_SIZE + END_OF_LINE_SIZE)





//============================================================================//
//===============              NMEA PAYLOAD FORMAT             ===============//
//============================================================================//



//----------------------------------------------------------------------------//
//----------                NMEA CHARACTERISTIC CHARS               ----------//

static const char  NMEA_START_OF_DATA     = '$';
static const char  NMEA_PARAM_SEPARATOR   = ',';
static const char  NMEA_END_OF_DATA       = '*';
static const char  NMEA_CR                = '\r';
static const char  NMEA_LF                = '\n';



//----------------------------------------------------------------------------//
//----------                    NMEA MTK COMMANDS                   ----------//


static const char* NMEA_MTK_COMMAND_START_FORMAT   = "PMTK";

static const char* NMEA_MTK_COMMAND_TEST                = "000";    //Performs a test

static const char* NMEA_MTK_COMMAND_HOT_START           = "101";    //Perform a HOT start
static const char* NMEA_MTK_COMMAND_WARM_START          = "102";    //Perform a WARM start
static const char* NMEA_MTK_COMMAND_COLD_START          = "103";    // Perform a COLD start
static const char* NMEA_MTK_COMMAND_RESET_COLD          = "104";    // Perform a system reset and then a COLD start
static const char* NMEA_MTK_COMMAND_ER_AID_DATA         = "120";    // Erase aiding data stored in flash memory
static const char* NMEA_MTK_COMMAND_ER_EPO_DATA         = "127";    // Erase EPO data stored in flash memory
static const char* NMEA_MTK_COMMAND_STANDBY             = "161";    // Standby configuration

static const char* NMEA_MTK_COMMAND_BAUDRATE            = "251";    // Set NMEA Baud rate

static const char* NMEA_MTK_COMMAND_SBAS                = "313";    // SBAS feature
static const char* NMEA_MTK_COMMAND_CONSTELLATION       = "353";    // Choose the constellation


//- - - - -                      NMEA MTK QUERIES                    - - - - -//

static const char* NMEA_MTK_COMMAND_QUERY_UTC_TIME      = "435";    // queries the current RTC UTC time

/**
 * TODO : Complete the command list
 */



struct nmea_mtk_sentence_t{
    char nmea_mtk_sentence[MTK_NMEA_SENTENCE_SIZE];
    size_t length;
    size_t char_left;
};

typedef struct nmea_mtk_sentence_t nmea_mtk_sentence_t;


struct nmea_mtk_payload_t{
    char nmea_mtk_payload[MTK_NMEA_PAYLOAD_SIZE];
    size_t length;
    size_t char_left;
};

typedef struct nmea_mtk_payload_t nmea_mtk_payload_t;



/**
 * Creates a MTK NMEA test payload
 * 
 * @param payload The destination where to store the payload.
 * 
 * @return -1 on fail, 0 on success
 */
int mtk_nmea_test_payload(nmea_mtk_payload_t* payload);



/**
 * Creates a NMEA MTK payload to set the baudrate of a device
 * 
 * @param payload   The destination where to store the payload.
 * @param baudrate  The baudrate to set
 * 
 * @return -1 on fail, 0 on success
 */
int mtk_nmea_set_baud_rate_payload(nmea_mtk_payload_t* payload, uint32_t baudrate);



/**
 * Creates a NMEA MTK UTC time query payload
 * 
 * @param payload   The destination where to store the payload.
 * 
 * @return -1 on fail, 0 on success
 */
int mtk_nmea_query_UTC_time_payload(nmea_mtk_payload_t* payload);





//----------------------------------------------------------------------------//
//----------                   NAVIGATION SYSTEMS                   ----------//

static const char* NMEA_TALKER_GPS         = "GP";     // GPS
static const char* NMEA_TALKER_GLONASS     = "GL";     // GLONASS
static const char* NMEA_TALKER_BEIDOU      = "BD";     // BEIDOU
static const char* NMEA_TALKER_GALILEO     = "GA";     // Galileo
static const char* NMEA_TALKER_GLOBAL      = "GN";     // Global Navigation/ Multi-constellation


//----------------------------------------------------------------------------//
//----------                  SENTENCE INDETIFIERS                  ----------//

static const char* NMEA_SENTENCE_ID_GGA    = "GGA";    // GNSS position fix data
static const char* NMEA_SENTENCE_ID_GLL    = "GLL";    // Geographic Position – Latitude & Longitude
static const char* NMEA_SENTENCE_ID_GSA    = "GSA";    // GNSS Dilution of Precision (DOP) and active satellites
static const char* NMEA_SENTENCE_ID_GSV    = "GSV";    // GNSS satellites in view.
static const char* NMEA_SENTENCE_ID_RMC    = "RMC";    // GNSS Recommended minimum navigation data
static const char* NMEA_SENTENCE_ID_VTG    = "VTG";    // Course Over Ground & Ground Speed
static const char* NMEA_SENTENCE_ID_ZDA    = "ZDA";    // Time & Date




// Initialisation

void init_sl869_v2();
void set_sl869_v2_baudrate();





//============================================================================//
//===============              NMEA OUTPUT PACKET              ===============//
//============================================================================//


//----------------------------------------------------------------------------//
//----------                       GGA PACKET                       ----------//


//TODO


//----------------------------------------------------------------------------//
//----------                       GLL PACKET                       ----------//


//TODO


//----------------------------------------------------------------------------//
//----------                       GSA PACKET                       ----------//


//TODO


//----------------------------------------------------------------------------//
//----------                       GSV PACKET                       ----------//


//TODO



//----------------------------------------------------------------------------//
//----------                       RMC PACKET                       ----------//


//TODO


//----------------------------------------------------------------------------//
//----------                       VTG PACKET                       ----------//


//TODO



//----------------------------------------------------------------------------//
//----------                       GRS PACKET                       ----------//


//TODO



//----------------------------------------------------------------------------//
//----------                       GST PACKET                       ----------//


//TODO


//----------------------------------------------------------------------------//
//----------                       ZDA PACKET                       ----------//


#define NB_ZDA_PACKET_PARAM         6


struct zda_packet_t{
    float utc_time;
    uint8_t utc_day;
    uint8_t utc_month;
    uint32_t utc_year;
    int8_t local_zone_hour;
    uint8_t local_zone_minutes;
};

typedef struct zda_packet_t zda_packet_t;


int extractZDAPacket(const char* payload, zda_packet_t* zda_packet);





#endif	/* NMEA_PAYLOAD_MAKER_H */
