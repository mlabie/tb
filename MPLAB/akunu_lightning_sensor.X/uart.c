#include "uart.h"


void uart_init(){
    
    uart1_init();
    
}

//============================================================================//
//==========                    UART1 Module                        ==========//
//============================================================================//

void uart1_init(){
    
    // taken from:
    // http://ww1.microchip.com/downloads/en/devicedoc/70000582e.pdf
    // p.18 and modified
    // and
    // https://www.microchip.com/forums/m626704.aspx
    
    U1MODEbits.UARTEN = 0;   // disable UART
    U1MODEbits.USIDL  = 0;   // continue in idle mode
    U1MODEbits.BRGH   = 0;   // Low Speed mode
    U1MODEbits.ABAUD  = 0;   // autobaud disabled
    U1MODEbits.UTXEN  = 0;   // Transmit enable
    U1MODEbits.URXEN  = 0;   // receive disabled
    U1MODEbits.MOD    = 0;   // 8-bits asynchronous mode.
    
    U1MODEHbits.BCLKSEL = 0; // system clock
    
    U1BRG = BRGVAL;          // Setting the beaudrate value
    
    U1MODEbits.UARTEN = 1;   // enable UART
    U1MODEbits.UTXEN = 1;    // enable transmitter
    U1MODEbits.URXEN = 1;    // enable receiver
}


//----------------------------------------------------------------------------//
//----------                        READ                            ----------//

char uart1_getc(){
    
    // taken from:
    // http://ww1.microchip.com/downloads/en/devicedoc/70000582e.pdf
    // p.25 and modified
    
    /* Check for receive errors */
    if(U1STAbits.FERR == 1){
        return 0;
    }
    
    /* Must clear the overrun error to keep UART receiving */
    if(U1STAbits.OERR == 1){
        U1STAbits.OERR = 0;
        return 0;
    }
    
    
    while(U1STAHbits.URXBE);
    
    return U1RXREG;
}


//----------------------------------------------------------------------------//
//----------                       WRITE                            ----------//

int uart1_putc(char c){
    
    // waits for the TX buffer to be empty before writing.
    while(!U1STAHbits.UTXBE || U1STAHbits.UTXBF);
    
    // Writes the character
    U1TXREG = c;
    
    return 0;
}


int uart1_write(char* s){
    char c;
    
    if(!s)
        return -1;
    
    while((c = *s)){
        if(uart1_putc(c))
            return -1;
        
        s++;  
    }
    
    return 0;
}

int uart1_writeln(char* s){
    int ret;
    
    ret = uart1_write(s);
    
    if(ret)
        return ret;
    
    // Prints the carriage return char and the end the line char
    uart1_putc('\r');
    uart1_putc('\n');
    
    return 0;
}