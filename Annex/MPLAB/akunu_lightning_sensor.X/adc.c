#include "adc.h"


void adc_init(){
    
    
    // Mostly taken from:
    // http://ww1.microchip.com/downloads/en/DeviceDoc/70005213f.pdf
    // pp. 58-61
    // and modified
    
    // ADC INITIALIZATION
    // Configure the I/O pins to be used as analog inputs.
    //ANSELAbits.ANSELA0 = 1;   // AN0/RA0 connected the dedicated core 0
    //TRISAbits.TRISA0   = 1;   // AN0/RA0 configured as an input
    
    
    
    // Configure the common ADC clock.
    ADCON3Hbits.CLKSEL  = 1; // clock from FRC oscillator
    ADCON3Hbits.CLKDIV  = 0; // no clock divider (1:1)
    ADCON2Lbits.SHRADCS = 0; // Shared ADC Core Input Clock Divider (1:2)
    
    // Configure the ADC reference sources.
    ADCON3Lbits.REFSEL = 0;         // AVdd as voltage reference
    ADCON1Hbits.SHRRES = 1;         // 8-bits resolution
    
    // Configure the integer of fractional output format.
    ADCON1Hbits.FORM = 0;   // integer format
    
    // Select single-ended input configuration and unsigned output format.
    ADMOD0Lbits.SIGN0 = 0;    // AN0/RA0
    
    
    // Enable the module.
    ADCON5Hbits.WARMTIME = 15;      // Set initialization time to maximum
    
    ADCON1Lbits.ADON     = 1;       // Turn on ADC module
    
    ADCON5Lbits.SHRPWR = 1;         // Turn on analog power for shared core
    
    while(ADCON5Lbits.SHRRDY == 0); // Wait when the shared core is ready for operation
    
    ADCON3Hbits.SHREN = 1;          // Turn on digital power to enable triggers to the shared core
    
    
    
    // Set same trigger source for all inputs to sample signals simultaneously.
    // software senstitive
    /*
    ADTRIG0Lbits.TRGSRC0 = 2;  // software level sensitive trigger for continious trigger
    ADTRIG0Lbits.TRGSRC1 = 2;  // software level sensitive trigger for continious trigger
    ADLVLTRGLbits.LVLEN0 = 1;  // channel 0 is level sensitive
    ADLVLTRGLbits.LVLEN1 = 1;  // channel 0 is level sensitive
    ADCON3Lbits.SWLCTRG  = 1;  // Generates continuous trigger
    */ 
    
    //ADTRIG0Lbits.TRGSRC0 = 2; // software level sensitive as trigger source
    ADTRIG0Lbits.TRGSRC0 = 21;    // Master SCCP2 PWM interrupt as trigger source
    
    ADLVLTRGLbits.LVLEN0 = 0;     // channel 0 is edge sensitive
    
}


inline int adc0_read_conv(){
    return ADCBUF0;
}

