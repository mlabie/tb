#include "utils.h"

int itoa(int i, char* buffer, uint16_t b){
    
    uint16_t cnt;
    unsigned char val;
    int tmp;
    
    if(!buffer)
        return -1;
    
    if(b < 2 || b > 16)
        return -1;
    
    cnt = 1;
    
    if(i < 0 && b == 10){
        i  *= -1;
        cnt++;
        buffer[0] = '-';
    }
    
    
    // Counting the number of digit
    tmp = i;
    
    while((tmp /= b) != 0)
        cnt++;
    
    // Putting the end of line char at the end of the buffer
    buffer[cnt--] = '\0';
    
    
    // Converting the value into a string
    do{
        val = i % b;
        i /= b;
        val = val >= 10 ? (val + 55) : (val + 48);
        buffer[cnt--] = (char)(val);
    }while(i != 0);
    
    
    return 0;
}



int uitoa(uint16_t i, char* buffer, uint16_t b){
    uint16_t cnt, tmp;
    unsigned char val;
    
    if(!buffer)
        return -1;
    
    if(b < 2 || b > 16)
        return -1;
    
    cnt = 1;
    
    
    // Counting the number of digit
    tmp = i;
    
    while((tmp /= b) != 0)
        cnt++;
    
    // Putting the end of line char at the end of the buffer
    buffer[cnt--] = '\0';
    
    
    // Converting the value into a string
    do{
        val = i % b;
        i /= b;
        val = val >= 10 ? (val + 55) : (val + 48);
        buffer[cnt--] = (char)(val);
    }while(i != 0);
    
    
    return 0;
}

int ultoa(uint32_t i, char* buffer, uint16_t b){
    uint16_t cnt;
    unsigned char val;
    uint32_t tmp;
    
    if(!buffer)
        return -1;
    
    if(b < 2 || b > 16)
        return -1;
    
    cnt = 1;
    
    
    // Counting the number of digit
    tmp = i;
    
    while((tmp /= (uint32_t)b) != 0)
        cnt++;
    
    // Putting the end of line char at the end of the buffer
    buffer[cnt--] = '\0';
    
    
    // Converting the value into a string
    do{
        val = (unsigned char)(i % (uint32_t)b);
        i /= (uint32_t)b;
        val = val >= 10 ? (val + 55) : (val + 48);
        buffer[cnt--] = (char)(val);
    }while(i != 0);
    
    
    return 0;
}