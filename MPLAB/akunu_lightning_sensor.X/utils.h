/* 
 * File:   utils.h
 * Author: Marc Labie
 *
 * Created on 11. juillet 2019, 14:03
 */

#ifndef UTILS_H
#define	UTILS_H


#include <xc.h>

/**
 * Converts an integer data type to a string data type. Based on the C itoa()
 * function. The buffer needs to be big enough to store the value. Count at least
 * one char for the end of line char '\0'.
 * If the base equals 10, than the output string will have a "-" at the begining.
 * 
 * @param i         The integer value to be converted
 * @param buffer    The buffer where to store the string value
 * @param b         The numerical base in which to representent the integer value.
 *                  Must be contain into [2, 16].
 * 
 * @return          0 on success, -1 on failure
 */
int itoa(int i, char* buffer, uint16_t b);


/**
 * Converts an unsigned integer data type to a string data type. Based on the C uitoa()
 * function. The buffer needs to be big enough to store the value. Count at least
 * one char for the end of line char '\0'.
 * 
 * @param i         The integer value to be converted
 * @param buffer    The buffer where to store the string value
 * @param b         The numerical base in which to representent the integer value.
 *                  Must be contain into [2, 16].
 * 
 * @return          0 on success, -1 on failure
 */
int uitoa(uint16_t i, char* buffer, uint16_t b);


/**
 * Converts an unsigned long data type to a string data type. Based on the C ultoa()
 * function. The buffer needs to be big enough to store the value. Count at least
 * one char for the end of line char '\0'.
 * 
 * @param i         The integer value to be converted
 * @param buffer    The buffer where to store the string value
 * @param b         The numerical base in which to representent the integer value.
 *                  Must be contain into [2, 16].
 * 
 * @return          0 on success, -1 on failure
 */
int ultoa(uint32_t i, char* buffer, uint16_t b);

#endif	/* UTILS_H */

