#include "nmea_payload_maker.h"
#include <stdlib.h>
#include <stdio.h>



//============================================================================//
//===============            MTK NMEA PAYLOAD TOOLS            ===============//
//============================================================================//


// source :
// http://people.cs.umu.se/isak/snippets/checksum.c
// and modified
static uint16_t checksum(void *buffer, size_t len, uint16_t seed)
{
    size_t i;
    uint8_t *buf;

    buf = (uint8_t *)buffer;

    for (i = 0; i < len; ++i)
        seed ^= (uint16_t)(*buf++);
    
    return seed;
}



static int create_MTK_NMEA_command(nmea_mtk_sentence_t* sentence, const char* command){
    
    size_t nb_written_char;
    
    if(!sentence || !command)
        return -1;
    
    // Adds the basic MTK command format with the command number to the sentence
    nb_written_char = snprintf(
        sentence->nmea_mtk_sentence,
        sentence->char_left,
        "%s%s",
        NMEA_MTK_COMMAND_START_FORMAT, // Starting with the MTK command format
        command                        // adding the command number
        );
    
    sentence->char_left -= nb_written_char;
    sentence->length    += nb_written_char;
    
    return 0;
}


static int add_mtk_nmea_command_param_s(nmea_mtk_sentence_t* sentence, const char* param){
    
    size_t nb_written_char;
    
    if(!sentence || !param)
        return -1;
    
    nb_written_char = snprintf(
        sentence->nmea_mtk_sentence + sentence->length,
        sentence->char_left,
        "%c%s",
        NMEA_PARAM_SEPARATOR,
        param
        );
    
    sentence->char_left -= nb_written_char;
    sentence->length    += nb_written_char;
    
    return 0;
}

static int add_mtk_nmea_command_param_i(nmea_mtk_sentence_t* sentence, int param){
    
    size_t nb_written_char;
    
    if(!sentence)
        return -1;
    
    nb_written_char = snprintf(
        sentence->nmea_mtk_sentence + sentence->length,
        sentence->char_left,
        "%c%d",
        NMEA_PARAM_SEPARATOR,
        param
        );
    
    sentence->char_left -= nb_written_char;
    sentence->length    += nb_written_char;
    
    return 0;
}


static int add_mtk_nmea_command_param_ui(nmea_mtk_sentence_t* sentence, unsigned int param){
    
    size_t nb_written_char;
    
    if(!sentence)
        return -1;
    
    nb_written_char = snprintf(
        sentence->nmea_mtk_sentence + sentence->length,
        sentence->char_left,
        "%c%u",
        NMEA_PARAM_SEPARATOR,
        param
        );
    
    sentence->char_left -= nb_written_char;
    sentence->length    += nb_written_char;
    
    return 0;
}


static int add_mtk_nmea_command_param_f(nmea_mtk_sentence_t* sentence, float param){
    
    size_t nb_written_char;
    
    if(!sentence)
        return -1;
    
    nb_written_char = snprintf(
        sentence->nmea_mtk_sentence + sentence->length,
        sentence->char_left,
        "%c%f",
        NMEA_PARAM_SEPARATOR,
        param
        );
    
    sentence->char_left -= nb_written_char;
    sentence->length    += nb_written_char;
    
    return 0;
}



static int create_MTK_NMEA_payload(nmea_mtk_payload_t* payload, const nmea_mtk_sentence_t* sentence){
    
    char checksum_buffer[NMEA_CHECKSUM_BUFF_SIZE];
    size_t sentence_size;
    size_t nb_written_char;
    uint16_t cs;
    
    
    if(!sentence || !payload)
        return -1;
    
    // Calculating the checksum
    //sentence_size = strlen(sentence->nmea_mtk_sentence);
    sentence_size = MTK_NMEA_SENTENCE_SIZE - sentence->char_left;
    cs = checksum((void*)sentence->nmea_mtk_sentence, sentence_size, 0);
    snprintf(checksum_buffer, NMEA_CHECKSUM_BUFF_SIZE, "%X", cs);
    
    // Creating the payload
    nb_written_char = snprintf(
        payload->nmea_mtk_payload,
        payload->char_left,
        "%c%s%c%s%c%c",
        NMEA_START_OF_DATA,             // Starting the payload with the NMEA starting char
        sentence->nmea_mtk_sentence,    // adding the sentence to the payload
        NMEA_END_OF_DATA,               // adds the end of data fields char
        checksum_buffer,                // adds the checksum
        NMEA_CR,                        // adds the carriage return char
        NMEA_LF                         // adds the line feed char
        );
    
    payload->char_left -= nb_written_char;
    payload->length    += nb_written_char;
    
    return 0;
}




//============================================================================//
//===============           MTK NMEA PAYLOAD BUILDER           ===============//
//============================================================================//



int mtk_nmea_test_payload(nmea_mtk_payload_t* payload){

    nmea_mtk_sentence_t sentence;
    
    if(!payload)
        return -1;
    
    payload->length = 0;
    sentence.length = 0;
    
    payload->char_left = MTK_NMEA_PAYLOAD_SIZE-1;
    sentence.char_left = MTK_NMEA_SENTENCE_SIZE-1;
    
    if(create_MTK_NMEA_command(&sentence, NMEA_MTK_COMMAND_TEST))
        return -1;
    
    if(create_MTK_NMEA_payload(payload, &sentence))
        return -1;
    
    return 0;
}



int mtk_nmea_set_baud_rate_payload(nmea_mtk_payload_t* payload, uint32_t baudrate){
    
    nmea_mtk_sentence_t sentence;
    
    if(!payload)
        return -1;
    
    payload->length = 0;
    sentence.length = 0;
    
    payload->char_left = MTK_NMEA_PAYLOAD_SIZE-1;
    sentence.char_left = MTK_NMEA_SENTENCE_SIZE-1;
    
    if(create_MTK_NMEA_command(&sentence, NMEA_MTK_COMMAND_BAUDRATE))
        return -1;
    
    if(add_mtk_nmea_command_param_ui(&sentence, baudrate))
        return -1;
    
    if(create_MTK_NMEA_payload(payload, &sentence))
        return -1;
    
    return 0;
}




int mtk_nmea_query_UTC_time_payload(nmea_mtk_payload_t* payload){
    
    nmea_mtk_sentence_t sentence;
    
    if(!payload)
        return -1;
    
    payload->length = 0;
    sentence.length = 0;
    
    payload->char_left = MTK_NMEA_PAYLOAD_SIZE-1;
    sentence.char_left = MTK_NMEA_SENTENCE_SIZE-1;
    
    if(create_MTK_NMEA_command(&sentence, NMEA_MTK_COMMAND_QUERY_UTC_TIME))
        return -1;
    
    if(create_MTK_NMEA_payload(payload, &sentence))
        return -1;
    
    return 0;
}



/*
int create_NMEA_payload(char* payload, const char* sentence){
    
    char checksum_buffer[NMEA_CHECKSUM_BUFF_SIZE];
    size_t sentence_size;
    uint16_t cs;
    
    if(!sentence || !payload)
        return -1;
    
    
    // Calculating the checksum
    sentence_size = strlen(sentence);
    cs = checksum((void*)sentence, sentence_size, 0);
    snprintf(checksum_buffer, NMEA_CHECKSUM_BUFF_SIZE, "%X", cs);
    
    
    // Creating the payload
    snprintf(payload,
             NMEA_PAYLOAD_SIZE,
             "%c%s%c%s%c%c",
             NMEA_START_OF_DATA,    // Starting the payload with the NMEA starting char
             sentence,              // adding the sentence to the payload
             NMEA_END_OF_DATA,      // adds the end of data fields char
             checksum_buffer,       // adds the checksum
             NMEA_CR,               // adds the carriage return char
             NMEA_LF                // adds the line feed char
            );
    
    
    return 0;
}*/



//============================================================================//
//===============              NMEA OUTPUT PACKET              ===============//
//============================================================================//


//----------------------------------------------------------------------------//
//----------                       GGA PACKET                       ----------//


//TODO


//----------------------------------------------------------------------------//
//----------                       GLL PACKET                       ----------//


//TODO


//----------------------------------------------------------------------------//
//----------                       GSA PACKET                       ----------//


//TODO


//----------------------------------------------------------------------------//
//----------                       GSV PACKET                       ----------//


//TODO



//----------------------------------------------------------------------------//
//----------                       RMC PACKET                       ----------//


//TODO


//----------------------------------------------------------------------------//
//----------                       VTG PACKET                       ----------//


//TODO



//----------------------------------------------------------------------------//
//----------                       GRS PACKET                       ----------//


//TODO



//----------------------------------------------------------------------------//
//----------                       GST PACKET                       ----------//


//TODO


//----------------------------------------------------------------------------//
//----------                       ZDA PACKET                       ----------//


int extractZDAPacket(const char* payload, zda_packet_t* zda_packet){
    
    char data[NMEA_SENTENCE_SIZE];
    size_t data_length, i;
    zda_packet_t zda;
    char *start_sentence, *end_sentence, *start_data, *end_data;
    
    
    
    if(!payload || !zda_packet)
        return -1;
    
    
    
    start_sentence = strchr(payload, NMEA_START_OF_DATA);
    end_sentence   = strchr(payload, NMEA_END_OF_DATA);
    
    
    if(!start_sentence || !end_sentence)
        return -1;
    
    
    // Check that the payload is a ZDA payload
    
    start_sentence += 1;
    
    end_data = strchr(payload, NMEA_PARAM_SEPARATOR);
    
    if(!end_data)
        return -1;
    
    data_length = end_data - start_sentence;
    strncpy(data, start_sentence, data_length);
    data[data_length] = '\0';
    
    if(!strstr(data, NMEA_SENTENCE_ID_ZDA))
        return -1;
    
    
    // Extract data
    
    for(i = 0; i < NB_ZDA_PACKET_PARAM; i++){
        
        start_data = end_data + 1;
        end_data   = strchr(start_data, NMEA_PARAM_SEPARATOR);
        
        if(!end_data){
            
            end_data = strchr(start_data, NMEA_END_OF_DATA);
            
            if(!end_data)
                return -1;
                
        }
        
        
        data_length = end_data - start_data;
        strncpy(data, start_data, data_length);
        data[data_length] = '\0';
        
        switch(i){
            case 0 :
                zda.utc_time = atof(data);
                break;
            case 1 :
                zda.utc_day = (uint8_t)(atoi(data));
                break;
            case 2 :
                zda.utc_month = (uint8_t)(atoi(data));
                break;
            case 3 :
                zda.utc_year = (uint32_t)(atoi(data));
                break;
            case 4 :
                zda.local_zone_hour = (int8_t)(atoi(data));
                break;
            case 5 :
                zda.local_zone_minutes = (uint8_t)(atoi(data));
                break;
            default :
                return -1;
        }
        
        start_data = end_data + 1;
        
    }
    
    
    memcpy(zda_packet, &zda, sizeof(zda_packet_t));
    
    return 0;
}







//============================================================================//
//===============                     MAIN                     ===============//
//============================================================================//

// main for test
int main(void){
    
    nmea_mtk_payload_t payload;
    int ret;
    zda_packet_t zda;
    
    const char *zda_test = "$GPZDA,201530.000,04,07,2016,-5,32*50\r\n";
    
    
    
    if(mtk_nmea_test_payload(&payload))
        printf("nupe\n");
    else{
        printf("payload : %s",payload.nmea_mtk_payload);
        printf("char left : %lu\n",payload.char_left);
        printf("length : %lu\n",payload.length);
    }
    
    
    if(mtk_nmea_set_baud_rate_payload(&payload, 115200))
        printf("nupe\n");
    else{
        printf("payload : %s",payload.nmea_mtk_payload);
        printf("char left : %lu\n",payload.char_left);
        printf("length : %lu\n",payload.length);
    }
    
    if(mtk_nmea_query_UTC_time_payload(&payload))
        printf("nupe\n");
    else{
        printf("payload : %s",payload.nmea_mtk_payload);
        printf("char left : %lu\n",payload.char_left);
    }
    
    
    ret = extractZDAPacket(zda_test, &zda);
    
    /*
    printf("UTC TIME : %f\n", zda.utc_time);
    printf("UTC day : %d\n", zda.utc_day);
    printf("UTC month : %d\n", zda.utc_month);
    printf("UTC year : %d\n", zda.utc_year);
    printf("UTC local zone hour : %d\n", zda.local_zone_hour);
    printf("UTC local zone minutes : %d\n", zda.local_zone_minutes);
    */

    
    return 0;
}
