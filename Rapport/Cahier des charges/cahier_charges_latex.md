```latex
\section{Contexte}

AKUNU est un projet né en 2013 dont l'objectif est le développement d'un réseau de capteurs à bas coût collectant des données atmosphériques pour les pays à faible revenu. Pour répondre à cette condition, le système doit être bon marché, simple à maintenir, hautement évolutif, modulable et à faible consommation d'énergie.\\
Un premier prototype du projet a déjà été développé à la heig-vd dans le cadre d'un travail de bachelor en 2018. Ce dernier dispose d'un GPS, d'un anémomètre, d'un thermomètre, d'un hygromètre, et d'un baromètre, mais il n'est pas encore en mesure de collecter des données associées aux champs électriques généré par la foudre.\\

\section{Objectif}

Le diplômant en charge du travail aura pour but de développer une extension au prototype existant permettant à celui-ci de mesurer les champs électriques associés à l’arc en retour des décharges atmosphériques à terre. Une antenne permettant de capturer le champ électrique lui sera fourni. Comme le montre la figure \ref{fig:station_AKUNU}, il devra mettre en place un système pour numériser la tension à la sortie de celle-ci et d'en retenir uniquement les données pertinentes, à savoir celles associées à ces décharges atmosphériques. Il sera de plus nécessaire d'estimer à l'aide d'un timer l'instant d'apparition du champ. Ces données seront alors transmises à la station qui les traitera.
\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{schema_bloc_station_AKUNU}
	\caption{Schéma bloc d'un dispositif AKUNU avec le capteur de champ électrique\label{fig:station_AKUNU}}
\end{figure}
À l'aide de ces données et de la géolocalisation des stations, le diplômant devra implémenter un algorithme basé sur la méthode dite de "Time of Arrival" permettant de localiser la foudre.\\
Enfin, le diplômant déploiera en Argentine (qui est l'un des membres fondateurs du projet AKUNU) le système existant accompagné du capteur de champs électriques développé. Il en effectuera le test et corrigera les éventuels erreurs et/ou problèmes rencontrés, et détaillera dans un rapport les améliorations nécessaires et souhaitables du système.
```
