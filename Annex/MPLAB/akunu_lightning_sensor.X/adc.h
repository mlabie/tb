/* 
 * File:   adc.h
 * Author: Marc Labie
 *
 * Created on 11. juillet 2019, 15:51
 */

#ifndef ADC_H
#define	ADC_H


#include <xc.h>


#define SAMPLING_RATE   2000000UL   // Sampling rate of the AD converter
//#define SAMPLING_RATE   100UL


/**
 * Initialisation function for the high-speed AD converter.
 */
void adc_init();


inline int adc0_read_conv();


#endif	/* ADC_H */

