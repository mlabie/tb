/* 
 * File:   irq.h
 * Author: Marc Labie
 *
 * Created on 16. juillet 2019, 14:36
 */

#ifndef IRQ_H
#define	IRQ_H


#include <xc.h>
#include "uart.h"
#include "utils.h"


/**
 * Initialise all the interrupt source needed.
 */
void irq_init(void);

/**
 * Enables all the interrupts initialised in the "irq_init" function
 */
void enableInterrupts(void);

/**
 * Disables all the interrupts initialised in the "irq_init" function
 */
void disableInterrupts(void);


//============================================================================//
//==========                      BASIC TIMER 1                     ==========//
//============================================================================//

/**
 * enable basic timer 1 interrupt
 */
void enableTimer1Interrupts(void);

/**
 * disable basic timer 1 interrupt
 */
void DisableTimer1Interrupts(void);


/**
 * basic timer 1 ISR
 */
void __attribute__((__interrupt__,no_auto_psv)) _T1Interrupt(void);


//============================================================================//
//==========                  External Interrupt 1                  ==========//
//============================================================================//

/**
 * enable External Interrupt 1 interrupt
 */
void enableINT1Interrupts(void);

/**
 * disable External Interrupt 1 interrupt
 */
void disableINT1Interrupts(void);

/**
 * External Interrupt 1 ISR
 */
void __attribute__((__interrupt__,no_auto_psv)) _INT1Interrupt(void);


//============================================================================//
//==========                       CCP TIMER 1                      ==========//
//============================================================================//

/**
 * enable CPT1 interrupt
 */
void enableCCT1Interrupts(void);

/**
 * disable CPT1 interrupt
 */
void disableCCT1Interrupts(void);

/**
 * CPT1 ISR
 */
void __attribute__((__interrupt__,no_auto_psv)) _CCT1Interrupt(void);


//============================================================================//
//==========                       CCP TIMER 2                      ==========//
//============================================================================//

/**
 * enable CPT2 interrupt
 */
void enableCCT2Interrupts(void);

/**
 * disable CPT2 interrupt
 */
void disableCCT2Interrupts(void);

/**
 * CPT2 ISR
 */
void __attribute__((__interrupt__,no_auto_psv)) _CCT2Interrupt(void);


//============================================================================//
//==========                       CCP TIMER 5                      ==========//
//============================================================================//

/**
 * enable CPT5 interrupt
 */
void enableCCT5Interrupts(void);

/**
 * disable CPT5 interrupt
 */
void disableCCT5Interrupts(void);

/**
 * CPT2 ISR
 */
void __attribute__((__interrupt__,no_auto_psv)) _CCT5Interrupt(void);



#endif	/* IRQ_H */

