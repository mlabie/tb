/* 
 * File:   timer.h
 * Author: Marc Labie
 *
 * Created on 12. juillet 2019, 14:39
 */

#ifndef TIMER_H
#define	TIMER_H

#include <xc.h>

#define NANOSECONDS     1000
#define MICROSECONDS    1000
#define MILLISECONDS    1000
#define SECONDS         60
#define MINUTES         60
#define HOURS           24

extern volatile unsigned int instructionCount;


/**
 * Initialise all the needed timer (below).
 */
void timer_init();


//============================================================================//
//==========              Master Bacis Timer Module                 ==========//
//============================================================================//

/**
 * Initialise the master basic timer module
 */
void timer1_init();

/**
 * Set the period value of the timer1.
 */
void timer1_load_period(uint16_t period);

/**
 * Gets the actual timer count value of the Timer1.
 */
uint16_t timer1_get_time();

/**
 * Starts the timer1
 */
void timer1_start();

/**
 * Stops the timer1
 */
void timer1_stop();

//============================================================================//
//==========                1 Second Period Timer                   ==========//
//============================================================================//


/**
 * Initialise the CCP1 Timer. This one is a periodic timer with a period of 1
 * second. It is set int 32-bits mode
 */
void CCP1_timer_init();


/**
 * Set the period value of the CCP1 timer.
 */
void CCP1_load_period(uint32_t tic);


/**
 * Gets the actual timer count value of the CCP1 Timer.
 */
uint32_t CCP1_get_time();

/**
 * Starts the CCP1 timer
 */
void CCP1_start();

/**
 * Stops the CCP1 timer
 */
void CCP1_stop();



//============================================================================//
//==========                  ADC Trigger Timer                     ==========//
//============================================================================//

/**
 * Initialise the CCP2 Timer. This one is used to trigger the AD convertion
 * to get a precise sampling rate. It is set in 16-bits mode.
 */
void CCP2_timer_init();

/**
 * Set the period value of the CCP2 timer.
 */
/**
 * Set the period value of the CCP2 timer.
 * 
 * @param tic : Number of clock cycle for the period
 */
void CCP2_load_period(uint16_t tic);

/**
 * Gets the actual timer count value of the CCP2 Timer.
 */
uint16_t CCP2_get_time();


/**
 * Starts the CCP2 timer
 */
void CCP2_start();

/**
 * Stops the CCP2 timer
 */
void CCP2_stop();



//============================================================================//
//==========           RTC < seconds for lightning sensor           ==========//
//============================================================================//


/**
 * Initialise the CCP3 Timer. This one is a timer that is reset every time a pps
 * signal is encountered. It runs at system clock frequency.
 */
void CCP3_timer_init();


/**
 * Gets the actual timer count value of the CCP3 Timer.
 */
uint32_t CCP3_get_time();

/**
 * Starts the CCP3 timer
 */
void CCP3_start();

/**
 * Stops the CCP3 timer
 */
void CCP3_stop();


//============================================================================//
//==========           RTC > seconds for lightning sensor           ==========//
//============================================================================//


/**
 * Initialise the CCP4 Timer. This one is a timer that increments every time he
 * detects a pps signal (pulse per second) from the gps.
 */
void CCP4_timer_init();


/**
 * Gets the actual timer count value of the CCP4 Timer.
 */
uint32_t CCP4_get_time();


/**
 * Set the timer time (not period).
 * 
 * @param time:     The time in second of the day
 */
void CCP4_set_time(uint32_t time);

/**
 * Starts the CCP4 timer
 */
void CCP4_start();

/**
 * Stops the CCP4 timer
 */
void CCP4_stop();


//============================================================================//
//==========                 Periodic ECHO Timer                    ==========//
//============================================================================//


/**
 * Initialise the CCP5 Timer. This one is a periodic timer with a period of 30
 * minutes. It send and echo to inform that it is still alive.
 */
void CCP5_timer_init();


/**
 * Set the period value of the CCP5 timer.
 */
void CCP5_load_period(uint32_t tic);


/**
 * Gets the actual timer count value of the CCP5 Timer.
 */
uint32_t CCP5_get_time();

/**
 * Starts the CCP5 timer
 */
void CCP5_start();

/**
 * Stops the CCP5 timer
 */
void CCP5_stop();



#endif	/* TIMER_H */

