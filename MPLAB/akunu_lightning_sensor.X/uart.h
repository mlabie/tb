/* 
 * File:   uart.h
 * Author: Marc Labie
 *
 * Created on 10. juillet 2019, 19:31
 */

#ifndef UART_H
#define	UART_H

#include <xc.h>
#include "pll.h"


#define BAUDRATE    460800UL
#define BRGVAL      (unsigned int)((FCY/(BAUDRATE*16))-1)


/**
 * Initialise all the the UART module (below)
 */
void uart_init();


//============================================================================//
//==========                    UART1 Module                        ==========//
//============================================================================//

/**
 * Initialisation function for the master core U1 uart.
 */
void uart1_init();


//----------------------------------------------------------------------------//
//----------                        READ                            ----------//

/**
 * Read a char from the UART1 receiver of the master core.
 * 
 * @return : the char that has been read.
 */
char uart1_getc();



//----------------------------------------------------------------------------//
//----------                       WRITE                            ----------//

/**
 * Writes a character on the uart1 of the master core.
 * 
 * @param c : Character to write
 * @return  : 0 on success
 */
int uart1_putc(char c);

/**
 * Writes a string on the uart1 of the master core. 
 * 
 * @param s : String to write
 * @return  : 0 on success, -1 on faillure
 */
int uart1_write(char* s);


/**
 * Writes a string on the uart1 of the master core, and add 
 * the carriage return and end of line chars at the end.
 * 
 * @param s : String to write
 * @return  : 0 on success, -1 on faillure
 */
int uart1_writeln(char* s);


#endif	/* UART_H */

