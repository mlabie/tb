#include "irq.h"

#include "timer.h"


void irq_init(void){
    
    // Taken from :
    // http://ww1.microchip.com/downloads/en/DeviceDoc/dsPIC33-PIC24-FRM,-Interrupts-DS70000600E.pdf
    // p.35
    // and modified
    
    INTCON1bits.NSTDIS = 0; // Interrupt nesting enabled here
    
    
    IPC0bits.T1IP    = 5;    // Set basic Timer 1 interrupt priority to 5
    IPC3bits.INT1IP  = 5;    // Set External Interrupt 1 interrupt priority to 5
    IPC1bits.CCT1IP  = 5;    // Set CCT1 Timer interrupt priority to 5
    IPC6bits.CCT2IP  = 4;    // Set CCT2 Timer interrupt priority to 4
    IPC11bits.CCT5IP = 4;    // Set CCT5 Timer interrupt priority to 4
    
    
    return;
}


void enableInterrupts(void){
    
    // Taken from :
    // http://ww1.microchip.com/downloads/en/DeviceDoc/dsPIC33-PIC24-FRM,-Interrupts-DS70000600E.pdf
    // p.35
    
    /* Enable level 1-7 interrupts */
    /* No restoring of previous CPU IPL state performed here */
    INTCON2bits.GIE = 1;
    return;
}


void disableInterrupts(void){
    
    // Taken from :
    // http://ww1.microchip.com/downloads/en/DeviceDoc/dsPIC33-PIC24-FRM,-Interrupts-DS70000600E.pdf
    // p.35
    
    /* Disable level 1-7 interrupts */
    /* No saving of current CPU IPL setting performed here */
    INTCON2bits.GIE = 0;
    return;
}



//============================================================================//
//==========                      BASIC TIMER 1                     ==========//
//============================================================================//

void enableTimer1Interrupts(void){
    IFS0bits.T1IF = 0;    // Reset basic Timer1 interrupt flag
    IEC0bits.T1IE = 1;    // Enable basic Timer1 interrupt
    return;
}


void disableTimer1Interrupts(void){
    IEC0bits.T1IE = 0;    // Disable basic Timer1 interrupt
    IFS0bits.T1IF = 0;    // Reset basic Timer1 interrupt flag
    return;
}


void __attribute__((__interrupt__,no_auto_psv)) _T1Interrupt(void){
    uart1_writeln("TEST");
    
    IFS0bits.T1IF = 0;  // Clear Timer1 interrupt
}


//============================================================================//
//==========                  External Interrupt 1                  ==========//
//============================================================================//

void enableINT1Interrupts(void){
    IFS0bits.INT1IF = 0;    // Reset External Interrupt 1 interrupt flag
    IEC0bits.INT1IE = 1;    // Enable External Interrupt 1 interrupt
    return;
}


void disableINT1Interrupts(void){
    IEC0bits.INT0IE = 0;    // Disable External Interrupt 1 interrupt
    IFS0bits.INT1IF = 0;    // Reset External Interrupt 1 interrupt flag
    return;
}


void __attribute__((__interrupt__,no_auto_psv)) _INT1Interrupt(void){
    char buffer[16];
    uint16_t time;
    
    time = TMR1;
    
    uitoa(time,buffer,10);
    uart1_writeln(buffer);
    
    IFS0bits.INT1IF = 0;    // Clear External Interrupt 1 interrupt
}


//============================================================================//
//==========                       CCP TIMER 1                      ==========//
//============================================================================//


void enableCCT1Interrupts(void){
    IFS0bits.CCT1IF = 0;    // Reset CCT1 Timer interrupt flag
    IEC0bits.CCT1IE = 1;    // Enable CCT1 Timer interrupt
    return;
}


void disableCCT1Interrupts(void){
    IEC0bits.CCT1IE = 0;    // Enable CCT1 Timer interrupt
    IFS0bits.CCT1IF = 0;    // Reset CCT1 Timer interrupt flag
    return;
}




void __attribute__((__interrupt__,no_auto_psv)) _CCT1Interrupt(void){
    /*char buffer[32];
    unsigned int time = CCP2_get_time();
    
    ultoa(time, buffer, 10);
    //uart1_write("Instruction in 1 sec :\t");
    uart1_writeln(buffer);
    */
    
    //uart1_writeln("a");
    
    //ADCON3Lbits.SWCTRG = 1;
    
    IFS0bits.CCT1IF = 0;    // Clear CCP1 Timer interrupt
}


//============================================================================//
//==========                       CCP TIMER 2                      ==========//
//============================================================================//


void enableCCT2Interrupts(void){
    IFS1bits.CCT2IF = 0;    // Reset CCT2 Timer interrupt flag
    IEC1bits.CCT2IE = 1;    // Enable CCT2 Timer interrupt
    return;
}


void disableCCT2Interrupts(void){
    IEC1bits.CCT2IE = 0;    // Enable CCT2 Timer interrupt
    IFS1bits.CCT2IF = 0;    // Reset CCT2 Timer interrupt flag
    return;
}




void __attribute__((__interrupt__,no_auto_psv)) _CCT2Interrupt(void){
    /*char buffer[32];
    unsigned int time = CCP2_get_time();
    
    ultoa(time, buffer, 10);
    //uart1_write("Instruction in 1 sec :\t");
    uart1_writeln(buffer);
    */
    
    //uart1_writeln("a");
    
    //ADCON3Lbits.SWCTRG = 1;
    
    IFS1bits.CCT2IF = 0;    // Clear CCP2 Timer interrupt
}



//============================================================================//
//==========                       CCP TIMER 5                      ==========//
//============================================================================//


void enableCCT5Interrupts(void){
    IFS2bits.CCT5IF = 0;    // Reset CCT5 Timer interrupt flag
    IEC2bits.CCT5IE = 1;    // Enable CCT5 Timer interrupt
    return;
}


void disableCCT5Interrupts(void){
    IEC2bits.CCT5IE = 0;    // Enable CCT5 Timer interrupt
    IFS2bits.CCT5IF = 0;    // Reset CCT5 Timer interrupt flag
    return;
}



void __attribute__((__interrupt__,no_auto_psv)) _CCT5Interrupt(void){
    
    static uint16_t cnt = 0;
    char buffer[16];
    
    uitoa(cnt, buffer, 10);
    
    
    uart1_write("echo");
    uart1_writeln(buffer);
    
    cnt++;
    
    IFS2bits.CCT5IF = 0;    // Clear CCP2 Timer interrupt
}