/* 
 * File:   akunu_lightning_sensor.h
 * Author: Marc Labie
 *
 * Created on 23. juillet 2019, 21:17
 */

#ifndef AKUNU_LIGHTNING_SENSOR_H
#define	AKUNU_LIGHTNING_SENSOR_H

#include <xc.h>
#include "adc.h"
//#include "circular_buffer.h"

#define US_IN_S                     1000000UL       // 1M us in 1 second


#define MAX_INPUT_VOLTAGE           3.3f
#define MIN_INPUT_VOLTAGE           0.0f
#define ZERO_EQUIV_VOLTAGE          1.65f
#define VOLTAGE_INPUT_OFFSET        0.6f


#define ADC_3_3_V_VAL               0
#define ADC_0_V_VAL                 255
#define ADC_1_65_V_VAL              127


//#define ADC_VOLTAGE_OFFSET_CORR    (-(VOLTAGE_INPUT_OFFSET/(MAX_INPUT_VOLTAGE/(float)(ADC_0_V_VAL))))
#define ADC_VOLTAGE_OFFSET_CORR    0


#define NEGATIVE_THRESHOLD_VAL      (ADC_1_65_V_VAL + ADC_VOLTAGE_OFFSET_CORR + 30)
#define POSITIVE_THRESHOLD_VAL      (ADC_1_65_V_VAL + ADC_VOLTAGE_OFFSET_CORR - 30)


#define US_BEFORE_LIGHTNING         10      // Number of microsec to sample before lightning
#define US_AFTER_LIGHTNING          440     // Number of microsec to sample after lightning


#define TOTAL_US                    US_BEFORE_LIGHTNING + US_AFTER_LIGHTNING


#define LIGHTNING_DATA_BEFORE       (US_BEFORE_LIGHTNING * SAMPLING_RATE)/US_IN_S
#define LIGHTNING_DATA_AFTER        (US_AFTER_LIGHTNING * SAMPLING_RATE)/US_IN_S


#define LIGHTNING_DATA_SIZE         LIGHTNING_DATA_BEFORE + LIGHTNING_DATA_AFTER    


// STATES
#define STATE_WAIT                  0
#define STATE_SAMPLING              1
#define STATE_DONE                  2


extern volatile uint8_t wait_pps;

/*
// The lightning event struct
struct lightning_event_t {
    uint64_t time;
    circular_buf_t waveform;
};


typedef struct lightning_event_t lightning_event_t;
*/


void callibrate_RTC_seconds();


#endif	/* AKUNU_LIGHTNING_SENSOR_H */

