/* 
 * File:   circular_buffer.h
 * Author: Marc Labie
 *
 * Created on 23. juillet 2019, 17:07
 */

#ifndef CIRCULAR_BUFFER_H
#define	CIRCULAR_BUFFER_H


#include <xc.h>
#include "uart.h"
#include "utils.h"
#include "akunu_lightning_sensor.h"


#define CIRCULAR_BUF_SIZE   (uint16_t)(LIGHTNING_DATA_SIZE)
//#define CIRCULAR_BUF_SIZE   120


// The hidden definition of our circular buffer structure
struct circular_buf_t {
    uint8_t  buffer[CIRCULAR_BUF_SIZE];
    uint16_t pos;
};

// Opaque circular buffer structure
typedef struct circular_buf_t circular_buf_t;


int circular_buf_init(circular_buf_t* cbuf);

inline int circular_buf_add_val(circular_buf_t* cbuf, uint8_t val);

int print_circular_buf(circular_buf_t* cbuf);

int print_circular_buf2(circular_buf_t* cbuf);

#endif	/* CIRCULAR_BUFFER_H */

