#include "IO_ports.h"


void io_ports_init(){
    
    timer1_ports_init();
    uart1_ports_init();
    adc_ports_init();
    led0_ports_init();
    int1_ports_init();
    
}


//============================================================================//
//==========                      BASIC TIMER1                      ==========//
//============================================================================//

void timer1_ports_init(){
    //*************************************************************
    // Unlock Registers
    //*************************************************************
    __builtin_write_RPCON(0x0000);
    
    _T1CKR=GPS_PPS_PIN; 
    
    //*************************************************************
    // lock Registers
    //*************************************************************
    __builtin_write_RPCON(0x0800);
}


//============================================================================//
//==========                        UART1 I/O                       ==========//
//============================================================================//

void uart1_ports_init(){
    
    // For this module, pin muxing configuration is necessary.
    // Code taken from :
    //http://ww1.microchip.com/downloads/en/DeviceDoc/dsPIC33-PIC24-FRM-IO-Ports-with-Edge-Detect-70005322b.pdf
    // p.14
    // and modified
    
    //*************************************************************
    // Unlock Registers
    //*************************************************************
    __builtin_write_RPCON(0x0000);
    
    // Assign U1Rx
    _U1RXR = UART1RX_PIN;
    
#if   UART1TX_PIN == RP32
    _RP32R = 1;
#elif UART1TX_PIN == RP33
    _RP33R = 1;
#elif UART1TX_PIN == RP34
    _RP34R = 1;
#elif UART1TX_PIN == RP35
    _RP35R = 1;
#elif UART1TX_PIN == RP36
    _RP36R = 1;
#elif UART1TX_PIN == RP37
    _RP37R = 1;
#elif UART1TX_PIN == RP38
    _RP38R = 1;
#elif UART1TX_PIN == RP39
    _RP39R = 1;
#elif UART1TX_PIN == RP40
    _RP40R = 1;
#elif UART1TX_PIN == RP41
    _RP41R = 1;
#elif UART1TX_PIN == RP42
    _RP42R = 1;
#elif UART1TX_PIN == RP43
    _RP43R = 1;
#elif UART1TX_PIN == RP44
    _RP44R = 1;
#elif UART1TX_PIN == RP45
    _RP45R = 1;
#elif UART1TX_PIN == RP46
    _RP46R = 1;
#elif UART1TX_PIN == RP47
    _RP47R = 1;
#endif
    
    //*************************************************************
    // lock Registers
    //*************************************************************
    __builtin_write_RPCON(0x0800);
            
}


//============================================================================//
//==========                         ADC I/O                        ==========//
//============================================================================//

void adc_ports_init(){
    
    // Configure the I/O pins to be used as analog inputs.
    ANSELAbits.ANSELA0 = 1;   // AN0/RA0 connected the dedicated core 0
    TRISAbits.TRISA0   = 1;   // AN0/RA0 configured as an input
    
}


//============================================================================//
//==========                        LED0 I/O                        ==========//
//============================================================================//

void led0_ports_init(){
    
    TRISBbits.TRISB3 = 0;   // RB3 as output
    LATBbits.LATB3   = 0;   // RB3 output to 0
    
}

void led0_on(){
    LATBbits.LATB3   = 1;   // RB3 output to 1
}

void led0_off(){
    LATBbits.LATB3   = 0;   // RB3 output to 0
}



//============================================================================//
//==========                 External Interrupt PIN                 ==========//
//============================================================================//

void int1_ports_init(){
    //*************************************************************
    // Unlock Registers
    //*************************************************************
    __builtin_write_RPCON(0x0000);
    
    
#if   INT1_PIN == RP32
    ANSELBbits.ANSELB0 = 0;   // RB0 analog input disabled.
    TRISBbits.TRISB0   = 1;   // RB0 as input
#elif INT1_PIN == RP33
    ANSELBbits.ANSELB1 = 0;   // RB1 analog input disabled.
    TRISBbits.TRISB1   = 1;   // RB1 as input
#elif INT1_PIN == RP34
    ANSELBbits.ANSELB2 = 0;   // RB2 analog input disabled.
    TRISBbits.TRISB2   = 1;   // RB2 as input
#elif INT1_PIN == RP35
    ANSELBbits.ANSELB3 = 0;   // RB3 analog input disabled.
    TRISBbits.TRISB3   = 1;   // RB3 as input
#elif INT1_PIN == RP36
    TRISBbits.TRISB4   = 1;   // RB4 as input
#elif INT1_PIN == RP37
    TRISBbits.TRISB5   = 1;   // RB5 as input
#elif INT1_PIN == RP38
    TRISBbits.TRISB6   = 1;   // RB6 as input
#elif INT1_PIN == RP39
    ANSELBbits.ANSELB7 = 0;   // RB7 analog input disabled.
    TRISBbits.TRISB7   = 1;   // RB7 as input
#elif INT1_PIN == RP40
    ANSELBbits.ANSELB8 = 0;   // RB8 analog input disabled.
    TRISBbits.TRISB8   = 1;   // RB8 as input
#elif INT1_PIN == RP41
    ANSELBbits.ANSELB9 = 0;   // RB9 analog input disabled.
    TRISBbits.TRISB9   = 1;   // RB9 as input
#elif INT1_PIN == RP42
    TRISBbits.TRISB10  = 1;   // RB10 as input
#elif INT1_PIN == RP43
    TRISBbits.TRISB11  = 1;   // RB11 as input
#elif INT1_PIN == RP44
    TRISBbits.TRISB12  = 1;   // RB12 as input
#elif INT1_PIN == RP45
    TRISBbits.TRISB13  = 1;   // RB13 as input
#elif INT1_PIN == RP46
    TRISBbits.TRISB14  = 1;   // RB14 as input
#elif INT1_PIN == RP47
    TRISBbits.TRISB15  = 1;   // RB15 as input
#endif
    
    
    RPINR6bits.ICM4R   = ICM4_PIN;
    RPINR6bits.TCKI4R  = ICM4_PIN;
    
    _INT1R = INT1_PIN;
    
    
    //*************************************************************
    // lock Registers
    //*************************************************************
    __builtin_write_RPCON(0x0800);
    
}