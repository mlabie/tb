/* 
 * File:   pll.h
 * Author: Marc Labie
 *
 * Created on 10. juillet 2019, 20:17
 */

#ifndef PLL_H
#define	PLL_H

#include <xc.h>

#define FCY         100000000UL

/**
 * Initialise the High-speed PLL oscillator module
 */
void pll_init();


#endif	/* PLL_H */

